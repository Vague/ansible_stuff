echo ============================================================================ 
echo .ssh setup
echo ============================================================================ 
# make sure your local ~/.ssh/config is setup to access the host first

# 1 = .ssh/config host name
# 2 = ssh key.pub to add
# 3 = any options for ssh 
set -x 
ssh "$1" $3  "[ ! -d ~/.ssh ] && mkdir .ssh ; echo "$(cat $2)" >> ~/.ssh/authorized_keys" 
echo verify:
set -x 
ssh "$1" ls .ssh
