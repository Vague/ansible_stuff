
echo ============================================================================ 
echo Installation
echo ============================================================================ 

# https://docs.ansible.com/ansible/latest/getting_started/get_started_ansible.html
grep debian /etc/os-release -q && {
	echo detected debian
	sudo apt install python3-pip
}

python3 -m pip install --user ansible

echo ============================================================================ 
echo "/home/$USER/.local/bin" needs to be in PATH
echo \# ansible install path
echo 'PATH=$PATH:/home/$USER/.local/bin' 
echo ============================================================================ 

echo now populating /etc/ansible/hosts
if [ ! -e /etc/ansible/hosts ] ; then
	cat myhosts > /etc/ansible/hosts
fi
